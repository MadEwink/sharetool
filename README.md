# Share Tool

This projects aims to create tools to share stuff in a fair way between peoples.

The use we had at first was to split an amount of money based on both the benefit each person took from something, and the level of income (it seemed more fair if someone earning twice as much paid more).

There are currently two dindependant parts to this project : a Python script, and a JavaScript tool.

## How it works

To compute the final share needed to be payed by each participant, this tool takes into account two variables :

* The **number of items** that each person gets
* The **income** of each person

The two have the same weight in the decision, and affect the result similarly.

### Examples results

You will find below several exemples of repartition.

| Name  | Items | Income | Final share |
|-------|-------|--------|-------------|
| Alice | 2     | 1000   | 66%         |
| Bob   | 1     | 1000   | 33%         |

| Name  | Items | Income | Final share |
|-------|-------|--------|-------------|
| Alice | 1     | 1000   | 33%         |
| Bob   | 1     | 2000   | 66%         |

| Name  | Items | Income | Final share |
|-------|-------|--------|-------------|
| Alice | 2     | 1000   | 40%         |
| Bob   | 1     | 1000   | 20%         |
| Clark | 1     | 2000   | 40%         |

## Python

At first we coded the algorithm in Python, because it is a great tool to prototype and test quickly.

Then, this tool has been reorganised a bit to handle csv data and all.

## JavaScript

This tool would be easier to use deployed on a web page, and since the calculation is pretty easy, it can all be done in JavaScript on a frontend-only site.

This tool aims to make it easier to use for anyone, with just a web browser.

## How to use, deploy, etc

You can find more information in the READMEs under the `js/` or `python/` folders.
