import * as app from './modules/app.js';

app.initialSetup();

let computeBtn = document.querySelector('.compute');
computeBtn.addEventListener('click', app.onClickCompute);

let personBtn = document.querySelector('.addPerson');
personBtn.addEventListener('click', app.onClickAddPerson);

let columnBtn = document.querySelector('.addColumn');
columnBtn.addEventListener('click', app.onClickAddColumn);
