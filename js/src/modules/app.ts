import { Compute } from './compute.js';

export function initialSetup() {
	let columnCount : number = 2;
	let peopleCount : number = 2;

	let peopleFields = document.getElementById("people") as HTMLDivElement;

	for (let i = 0 ; i < peopleCount ; i ++) {
		addPerson(peopleFields, columnCount);
	}
}

export function onClickCompute() {
    let peopleFields = document.getElementById("people") as HTMLDivElement;
    let compute = new Compute(peopleFields);
    compute.compute();
    compute.writeResults(peopleFields);
}

export function onClickAddPerson() {
    let peopleFields = document.getElementById("people") as HTMLDivElement;
    let columnCount = Compute.countColumns(peopleFields);
    addPerson(peopleFields, columnCount);
}

function addPerson(peopleFields : HTMLDivElement, columnCount : number) {
    let newPerson = document.createElement("div");
    newPerson.setAttribute("class", "person");
    
    let nameField = document.createElement("textarea");
    nameField.setAttribute("class", "name");
    newPerson.appendChild(nameField);

    for (let i = 0 ; i < columnCount ; i++) {
        let valueField = document.createElement("textarea");
        valueField.setAttribute("class", "value");
        newPerson.appendChild(valueField);
    }

    let outputField = document.createElement("output");
    newPerson.appendChild(outputField);

    peopleFields.appendChild(newPerson);
}

export function onClickAddColumn() {
    let peopleFields = document.getElementById("people") as HTMLDivElement;
    for (let personField of peopleFields.getElementsByClassName("person")) {
        let newValue = document.createElement("textarea");
        newValue.setAttribute("class", "value");

        let outputField = personField.lastElementChild;
        personField.insertBefore(newValue, outputField);
    }
}
