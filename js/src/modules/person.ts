export class Person {
    name : string
    values : Array<number>
    index : number | undefined

    constructor(name : string, values : Array<number>, index : number | undefined = undefined) {
        this.name = name;
        this.values = values;
        this.index = index;
    }
}
