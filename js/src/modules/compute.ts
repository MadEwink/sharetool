import { Person } from './person.js';

export class Compute {
    peopleArray : Array<Person>
    valueLength : number

    constructor(peopleFields : HTMLDivElement) {
        this.peopleArray = [];
        this.valueLength = Compute.countColumns(peopleFields);

        for (let personFields of peopleFields.getElementsByClassName("person")) {
            let nameTextAreas = personFields.getElementsByClassName("name") as HTMLCollectionOf<HTMLTextAreaElement>;
            if (nameTextAreas.length === 0) {
                // TODO : raise error
            }
        let name = nameTextAreas[0].value;

        let values = [];
        for (let valueField of personFields.getElementsByClassName("value") as HTMLCollectionOf<HTMLTextAreaElement>) {
            // TODO : warn if valueField.value is zero
            values.push(Number(valueField.value));
        }
        if (this.valueLength != values.length) {
            // TODO : raise error
        }
            this.peopleArray.push(new Person(name, values));
        }
    }

    static countColumns(peopleFields : HTMLDivElement) : number {
        let personFields = peopleFields.getElementsByClassName("person")[0];
        let count = personFields.getElementsByClassName("value").length;
        return count;
    }

    // compute repartition
    compute() {
        // compute all value sums
        let sumsArray = Array(this.valueLength).fill(0);
        
        for (let person of this.peopleArray) {
            for (let i = 0 ; i < this.valueLength ; i++) {
                sumsArray[i] += person.values[i];
            }
        }

        // compute temporary index and its sum
        let tempIndexArray = Array(this.peopleArray.length).fill(1);
        let tempIndexSum = 0;
        for (let i = 0 ; i < this.peopleArray.length ; i++) {
            for (let j = 0 ; j < this.valueLength ; j++) {
                // TODO : check if sumsArray[j] is zero
                tempIndexArray[i] *= (this.peopleArray[i].values[j] / sumsArray[j]);
            }
            tempIndexSum += tempIndexArray[i];
        }

        // compute index for each person
        for (let i = 0 ; i < this.peopleArray.length ; i++) {
            this.peopleArray[i].index = tempIndexArray[i] / tempIndexSum;
        }
    }

    writeResults(peopleFields : HTMLDivElement) {
        let personFieldArray = peopleFields.getElementsByClassName("person");
        for (let i = 0 ; i < personFieldArray.length ; i++) {
            let outputField = personFieldArray[i].getElementsByTagName("output")[0];
            outputField.value = this.peopleArray[i].index.toString();
        }
    }
}
