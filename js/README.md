# Share Tool - JS

This simple tool aims to ease equitable sharing computation between people.

## Installation and deployment

All you need is to run a web server such as apache2 targeting this folder, and connect to it, usually by typing `localhost` in any web browser.

### Docker

You will find in this repository a `docker-compose.yaml` and a `Dockerfile`.

The Compose file sets up an Nginx server with the static files mounted as a volume. This is the recommended method. It opens the port `45126` (chosen arbitrarily, feel free to change it).

You can run it using the following command :

```sh
docker-compose up -d
```

The Dockerfile copies the static files into an image based on Nginx. This means that you will have to rebuild the image everytime you make a modification to the code. This also means it produces bigger images. This method isn't recommended, but is provided as a way to create a all-in-one Docker image. It is especially useful if you've installed `docker` but not `docker-compose`.

You can run it using the following commands :

```sh
docker build -t sharetool .
docker run -d -P --name=sharetool sharetool
```
