# Share Tool - py

This simple tool aims to ease equitable sharing computation between people.

## How to use

1. Create a `data` folder
2. Create a file named `people.csv` in that folder
3. Fill in the file with entries under the following format : `name, claims, income` where :
  * `name` is a string that can be enclosed in double quotes `"`
  * `claims` and `income` are floats (that will be parsed with the `float` method) that should be positive
4. From the project root, run `python src/main.py`
