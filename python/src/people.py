import csv

class Person:
    def __init__(self, name, claims, income):
        self.name = name
        self.claims = claims
        self.income = income
        self.index = -1
    def print(self):
        if (self.index > 0):
            print("{} : {}, {} -> {}".format(self.name, self.claims, self.income, self.index))
        else:
            print("{} : {}, {}".format(self.name, self.claims, self.income))

class People:
    def __init__(self):
        self.container = []

    def parseCSVstr(self, content):
        spamreader = csv.reader(content, delimiter=',', quotechar='"')
        for row in spamreader:
            person = Person(row[0], float(row[1]), float(row[2]))
            self.container.append(person)

    def print(self):
        for person in self.container:
            person.print()

    def __iter__(self):
        self.index = 0
        return self

    def __next__(self):
        if self.index >= len(self.container):
            raise StopIteration
        v = self.container[self.index]
        self.index += 1
        return v

